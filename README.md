# Criação de um servidor Web com Jboss (wildfly)

Criação do zero para criar um servidor web simples com jboss (Wildfly)

Será usado o terraform para criar a infraestutura e o ansible para realizar as alterações nas máquinas.

# Pré-requisitos
- Possuir o **terraform** e **ansible** instalados.
- Exportar a chave de acesso para o terraform.
```shell
$ export AWS_ACCESS_KEY_ID="anaccesskey"
$ export AWS_SECRET_ACCESS_KEY="asecretkey"
```

# Forma de execução

Realizar o clone desse repositório, acessar a pasta terraform e executar o comando terraform apply

```shell
$ terraform apply
```
